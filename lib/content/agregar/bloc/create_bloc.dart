import 'dart:async';
import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:equatable/equatable.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:image_picker/image_picker.dart';
import 'package:meta/meta.dart';

part 'create_event.dart';
part 'create_state.dart';

class CreateBloc extends Bloc<CreateEvent, CreateState> {
  File? _selectedPicture;
  CreateBloc() : super(CreateInitial()) {
    on<OnCreateTakePictureEvent>(_takePicture);
    on<OnCreateSaveDataEvent>(_saveData);
  }

  FutureOr<void> _saveData(OnCreateSaveDataEvent event, emit) async {
    emit(CreateLoadingState());
    bool saved = await _saveFshare(event.dataToSave);
    emit(saved ? CreateSuccessState() : CreateFshareErrorState());
  }

  Future<bool> _saveFshare(Map<String, dynamic> dataToSave) async {
    try {
      //subir imagen al bucket
      String _imageUrl = await _uploadPictureToStorage();
      if (_imageUrl != "") {
        //si se subio la imagen se acutaliza el mapa
        dataToSave["picture"] = _imageUrl;
        dataToSave["publishedAt"] = Timestamp.fromDate(DateTime.now());
        dataToSave["stars"] = 0;
        dataToSave["username"] = FirebaseAuth.instance.currentUser!.displayName;
        dataToSave["creator_id"] = FirebaseAuth.instance.currentUser!.uid;
      } else {
        return false;
      }
      //guardar Fshare en cloud
      var docRef =
          await FirebaseFirestore.instance.collection("fshare").add(dataToSave);
      // actualziar lista en usuario
      return await _updateUserDocumentReference(docRef.id);
    } catch (e) {
      return false;
    }
  }

  Future<String> _uploadPictureToStorage() async {
    try {
      var stamp = DateTime.now();
      if (_selectedPicture == null) {
        return "";
      }
      UploadTask task = FirebaseStorage.instance
          .ref("fshares/imagen_$stamp.png")
          .putFile(_selectedPicture!);

      await task;
      return await (await task.storage)
          .ref("fshares/imagen_$stamp.png")
          .getDownloadURL();
    } catch (e) {
      return "";
    }
  }

  Future<bool> _updateUserDocumentReference(String fshareId) async {
    try {
      // query para traer el documento con el id del usuario autenticado
      var queryUser = await FirebaseFirestore.instance
          .collection("user")
          .doc("${FirebaseAuth.instance.currentUser!.uid}");
      // query para sacar la data del documento
      var docsRef = await queryUser.get();
      print(docsRef.data()?["fotosListId"]);
      List<dynamic> listIds = docsRef.data()?["fotosListId"];
      listIds.add(fshareId);

      await queryUser.update({"fotosListId": listIds});
      return true;
    } catch (e) {
      return false;
    }
  }

  Future<void> _takePicture(event, emit) async {
    emit(CreateLoadingState());
    await _getImage();
    if (_selectedPicture != null) {
      emit(CreatePictureChangedState(picture: _selectedPicture!));
    } else {
      emit(CreatePictureErrorState());
    }
  }

  Future<void> _getImage() async {
    final pickedFile = await ImagePicker().pickImage(
        source: ImageSource.gallery,
        maxHeight: 720,
        maxWidth: 720,
        imageQuality: 85);
    if (pickedFile != null) {
      _selectedPicture = File(pickedFile.path);
    } else {
      _selectedPicture = null;
    }
  }
}
