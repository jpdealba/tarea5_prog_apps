part of 'update_photo_bloc.dart';

@immutable
abstract class UpdatePhotoEvent extends Equatable {
  const UpdatePhotoEvent();
  @override
  List<Object> get props => [];
}

//cunaod el usuario hace click en foto
class OnUpdatePictureEvent extends UpdatePhotoEvent {}
