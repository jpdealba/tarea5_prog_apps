import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:image_picker/image_picker.dart';
import 'package:meta/meta.dart';

part 'update_photo_event.dart';
part 'update_photo_state.dart';

class UpdatePhotoBloc extends Bloc<UpdatePhotoEvent, UpdatePhotoState> {
  File? _selectedPicture;
  UpdatePhotoBloc() : super(UpdatePictureInitial()) {
    on<OnUpdatePictureEvent>(_takePicture);
  }

  Future<void> _takePicture(event, emit) async {
    emit(UpdatePictureLoading());
    await _getImage();
    if (_selectedPicture != null) {
      emit(UpdatePictureChangedState(picture: _selectedPicture!));
    } else {
      emit(UpdatePictureErrorState());
    }
  }

  Future<void> _getImage() async {
    final pickedFile = await ImagePicker().pickImage(
        source: ImageSource.gallery,
        maxHeight: 720,
        maxWidth: 720,
        imageQuality: 85);
    if (pickedFile != null) {
      _selectedPicture = File(pickedFile.path);
    } else {
      _selectedPicture = null;
    }
  }
}
