part of 'update_photo_bloc.dart';

// class UpdatePhotoInitial extends UpdatePhotoState {}

abstract class UpdatePhotoState extends Equatable {
  const UpdatePhotoState();

  @override
  List<Object> get props => [];
}

//estados de seleccionar foto

class UpdatePictureInitial extends UpdatePhotoState {}

class UpdatePictureLoading extends UpdatePhotoState {}

class UpdatePictureErrorState extends UpdatePhotoState {}

class UpdatePictureChangedState extends UpdatePhotoState {
  final File picture;

  UpdatePictureChangedState({required this.picture});
  @override
  List<Object> get props => [picture];
}
