import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class ItemMine extends StatefulWidget {
  final Map<String, dynamic> publicFData;
  final String document_id;
  dynamic Function(dynamic, dynamic) updateComp;

  ItemMine(
      {Key? key,
      required this.publicFData,
      required this.document_id,
      required this.updateComp})
      : super(key: key);

  @override
  State<ItemMine> createState() => _ItemMineState();
}

class _ItemMineState extends State<ItemMine> {
  @override
  Widget build(BuildContext context) {
    bool _defaultSwitchValue = widget.publicFData["public"];
    return Padding(
      padding: EdgeInsets.all(10.0),
      child: Column(
        mainAxisSize: MainAxisSize.max,
        children: [
          Container(
            width: MediaQuery.of(context).size.width * 0.8,
            child: Container(
              child: Card(
                clipBehavior: Clip.antiAlias,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    AspectRatio(
                      aspectRatio: 16 / 9,
                      child: Image.network("${widget.publicFData["picture"]}",
                          fit: BoxFit.cover),
                    ),
                    SwitchListTile(
                        title:
                            Text("${widget.publicFData["title"]}", maxLines: 1),
                        subtitle: Text(
                            "${widget.publicFData["publishedAt"].toDate()}",
                            maxLines: 2),
                        secondary: CircleAvatar(
                            child: Text(
                                "${widget.publicFData["username"].toString()[0]}")),
                        value: _defaultSwitchValue,
                        onChanged: (newValue) async {
                          _defaultSwitchValue = newValue;
                          var queryFshare = await FirebaseFirestore.instance
                              .collection("fshare")
                              .doc("${widget.document_id}");

                          await queryFshare.update({"public": newValue});
                          setState(() {});
                        }),
                    MaterialButton(
                      onPressed: () {
                        widget.updateComp(
                            widget.publicFData, widget.document_id);
                      },
                      child: Container(
                        width: 80,
                        height: 30,
                        decoration: BoxDecoration(
                            color: Colors.grey,
                            borderRadius: new BorderRadius.circular(8.0)),
                        child: Center(
                            child: Text(
                          "Editar",
                          style: TextStyle(color: Colors.white),
                        )),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
