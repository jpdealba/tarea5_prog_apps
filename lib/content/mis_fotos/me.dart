import 'dart:io';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutterfire_ui/firestore.dart';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:foto_share/content/mis_fotos/item_mine.dart';

import 'bloc/update_photo_bloc.dart';

class Me extends StatefulWidget {
  const Me({Key? key}) : super(key: key);

  @override
  State<Me> createState() => _MeState();
}

class _MeState extends State<Me> {
  Map<String, dynamic>? edit_event;
  String? editable_document_id;
  var _titleC = TextEditingController();
  var new_date;
  File? image;
  bool _defaultSwitchValue = false;
  updateComp(event, document_id) {
    if (document_id == editable_document_id) {
      editable_document_id = null;
      edit_event = null;
      _titleC.clear();
      new_date = null;
      _defaultSwitchValue = false;
      image = null;
    } else {
      editable_document_id = document_id;
      edit_event = event;
      _titleC..text = "${event["title"]}";
      new_date = Timestamp.fromDate(DateTime.now());
      _defaultSwitchValue = event["public"];
    }
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<UpdatePhotoBloc, UpdatePhotoState>(
      listener: (context, state) {
        if (state is UpdatePictureErrorState) {
          ScaffoldMessenger.of(context).showSnackBar(
              SnackBar(content: Text("Error al cargar la foto...")));
        } else if (state is UpdatePictureChangedState) {
          image = state.picture;
        }
      },
      builder: (context, state) {
        return SingleChildScrollView(
          scrollDirection: Axis.vertical,
          child: Wrap(
            children: [
              Column(
                children: [
                  Container(
                    height: 300,
                    width: MediaQuery.of(context).size.width,
                    child: FirestoreListView(
                      scrollDirection: Axis.horizontal,
                      query: FirebaseFirestore.instance
                          .collection("fshare")
                          .where("creator_id",
                              isEqualTo:
                                  FirebaseAuth.instance.currentUser!.uid),
                      itemBuilder: (BuildContext context,
                          QueryDocumentSnapshot<Map<String, dynamic>>
                              document) {
                        return ItemMine(
                            publicFData: document.data(),
                            document_id: document.id,
                            updateComp: updateComp);
                      },
                    ),
                  ),
                  if (edit_event != null)
                    Divider(
                      thickness: 2,
                    ),
                  if (edit_event != null)
                    Container(
                      width: MediaQuery.of(context).size.width * 0.8,
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          if (image != null)
                            AspectRatio(
                                aspectRatio: 16 / 9,
                                child: MaterialButton(
                                  onPressed: () {
                                    BlocProvider.of<UpdatePhotoBloc>(context)
                                        .add(OnUpdatePictureEvent());
                                  },
                                  child: Image.file(image!, fit: BoxFit.cover),
                                ))
                          else
                            AspectRatio(
                                aspectRatio: 16 / 9,
                                child: MaterialButton(
                                  onPressed: () {
                                    BlocProvider.of<UpdatePhotoBloc>(context)
                                        .add(OnUpdatePictureEvent());
                                  },
                                  child: Image.network(
                                      "${edit_event!["picture"]}",
                                      fit: BoxFit.cover),
                                )),
                          Padding(
                            padding: const EdgeInsets.only(top: 8.0),
                            child: TextField(
                                controller: _titleC,
                                decoration: InputDecoration(
                                    label: Text("Title"),
                                    border: OutlineInputBorder())),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 8.0),
                            child: Text("New Date: ${DateTime.now()}"),
                          ),
                          Row(
                            children: [
                              Expanded(
                                child: Align(
                                    alignment: Alignment.topLeft,
                                    child: Text("Publicar: ")),
                              ),
                              Expanded(
                                child: Align(
                                  alignment: Alignment.topRight,
                                  child: Switch(
                                      value: _defaultSwitchValue,
                                      onChanged: (newVal) {
                                        _defaultSwitchValue = newVal;
                                        setState(() {});
                                      }),
                                ),
                              ),
                            ],
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: MaterialButton(
                              onPressed: () async {
                                var queryFshare = await FirebaseFirestore
                                    .instance
                                    .collection("fshare")
                                    .doc("$editable_document_id");

                                String _imageUrl =
                                    await _uploadPictureToStorage(
                                        image, edit_event!["picture"]);
                                print(_imageUrl);
                                await queryFshare.update({
                                  "public": _defaultSwitchValue,
                                  "title": _titleC.text,
                                  "publishedAt": new_date,
                                  "picture": _imageUrl
                                });

                                editable_document_id = null;
                                edit_event = null;
                                _titleC.clear();
                                new_date = null;
                                _defaultSwitchValue = false;
                                setState(() {});
                              },
                              child: Container(
                                width: 250,
                                height: 40,
                                decoration: BoxDecoration(
                                    color: Colors.green,
                                    borderRadius:
                                        new BorderRadius.circular(8.0)),
                                child: Center(
                                    child: Text(
                                  "Guardar",
                                  style: TextStyle(color: Colors.white),
                                )),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                ],
              )
            ],
          ),
        );
      },
    );
  }
}

Future<String> _uploadPictureToStorage(file_image, old_image) async {
  try {
    var stamp = DateTime.now();
    if (file_image == null) {
      return old_image;
    }
    UploadTask task = FirebaseStorage.instance
        .ref("fshares/imagen_$stamp.png")
        .putFile(file_image!);

    await task;
    return await (await task.storage)
        .ref("fshares/imagen_$stamp.png")
        .getDownloadURL();
  } catch (e) {
    return old_image;
  }
}
