import 'dart:io';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:share_plus/share_plus.dart';
import 'package:path_provider/path_provider.dart';

class ItemPublished extends StatefulWidget {
  final Map<String, dynamic> publicFData;

  ItemPublished({Key? key, required this.publicFData}) : super(key: key);

  @override
  State<ItemPublished> createState() => _ItemPublishedState();
}

class _ItemPublishedState extends State<ItemPublished> {
  @override
  Widget build(BuildContext context) {
    print(widget.publicFData["publishedAt"]);
    return Padding(
      padding: EdgeInsets.all(18.0),
      child: Card(
        clipBehavior: Clip.antiAlias,
        child: Column(
          children: [
            AspectRatio(
              aspectRatio: 16 / 9,
              child: Image.network("${widget.publicFData["picture"]}",
                  fit: BoxFit.cover),
            ),
            ListTile(
              leading: CircleAvatar(
                  child:
                      Text("${widget.publicFData["username"].toString()[0]}")),
              title: Text("${widget.publicFData["title"]}"),
              subtitle: Text("${widget.publicFData["publishedAt"].toDate()}"),
              trailing: Wrap(
                children: [
                  IconButton(
                    onPressed: () {},
                    icon: Icon(Icons.star_outlined, color: Colors.green),
                    tooltip: "Likes: ${widget.publicFData["stars"]}",
                  ),
                  IconButton(
                    onPressed: () async {
                      final url_image = widget.publicFData["picture"];
                      final url = Uri.parse(url_image);
                      final response = await http.get(url);
                      final bytes = response.bodyBytes;
                      final temp = await getTemporaryDirectory();
                      final path = '${temp.path}/image.jpg';
                      File(path).writeAsBytesSync(bytes);
                      await Share.shareFiles([path],
                          text: "${widget.publicFData["title"]}" +
                              "\n" +
                              "${widget.publicFData["publishedAt"].toDate()}");
                    },
                    icon: Icon(Icons.share, color: Colors.grey),
                    tooltip: "Compartir",
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
